const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')
const Dotenv = require('dotenv-webpack');
const  {VueLoaderPlugin}  = require("vue-loader");
const os = require("os");
// cpu核数
const threads = os.cpus().length;

module.exports = {
  mode: "development",
  // 指定入口文件
  entry: "./src/main.ts",

  // 开发模式使用，方便查错误
  devtool: "inline-source-map",
  cache: {
    type: 'filesystem',
    allowCollectingMemory: true
  },
  // 配置服务器
  devServer: {
    static: {
      directory: path.resolve(__dirname, "dist"),
      staticOptions: {},
      publicPath: "./",
      serveIndex: true,
      watch: true,
    },
    open: false,
    port:4080,
    hot: true,
    client: {
      overlay: {
        warnings: false,
        errors: true,
      },
    },
    proxy: {
      "/proxy": {
        target: "",
        changeOrigin: true,
        pathRewrite: {
          "^/proxy": "",
        },
      },
    },
  },
  // 指定打包文件所在目录
  output: {
    // path: path.resolve(__dirname, "dist"),
    // filename: "bundle.js",
    // environment: {
    //   arrowFunction: false, // 关闭webpack的箭头函数，可选
    // },
    filename: '[name].[contenthash].js', // 使用contenthash确保文件更改时缓存失效
    path: path.resolve(__dirname, 'dist'), // 输出目录
    clean: true, // 清除输出目录
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 20000,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      cacheGroups: {
        default: {
          priority: -20,
          reuseExistingChunk: true,
        },
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          name: 'vendors',
          chunks: 'all',
        },
        elementPlus: {
          test: /[\\/]node_modules[\\/](element-plus)[\\/]/,
          priority: -9,
          name: 'element-plus',
          chunks: 'all',
        },
        // 添加更多的cacheGroups以进一步细分拆分
        asyncViews: {
          test: /[\\/]Views[\\/]/, // 适当地调整正则表达式以匹配您的视图文件路径
          priority: -9,
          name: 'async-views',
          chunks: 'all',
        },
      },
    },
  },
  
  
  // 用来设置引用模块
  resolve: {
    alias: {
      vue$: "vue",
      jquery: "jquery/src/jquery",
      '@': path.resolve(__dirname, 'src'),
      '@images': path.resolve(__dirname, './src/assets'),
      '@models': path.resolve(__dirname, './src/assets/Models')
    },
    extensions: [".ts", ".tsx", ".js", ".vue", ".json"]
  },

  // 配置webpack的loader
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: "thread-loader", // 开启多进程
             options: {
               workers: threads, // 数量
             },
           },
          'babel-loader'
        ]
      },
      {
        test: /\.vue$/,
        use: "vue-loader",
      },
      {
        test: /\.ts?$/,
        use: {
          loader: "ts-loader",
          options: {
            appendTsSuffixTo: [/\.vue$/],
            transpileOnly: true,
          },
        },
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: [
          {
            loader: 'url-loader', // 使用file-loader来处理图片文件
            options: {
              esModule: false,
              limit: 8 * 1024, // 将小于8kb的图片用based64处理
              name: '[name].[ext]', // 保持原始文件名和扩展名
              outputPath: 'images/' // 输出到指定目录下的images文件夹
            }
          }
        ],
        type: "javascript/auto", //转换 json 为 js
      },
      {
        test: /\.(glb|gltf|FBX|fbx|obj)$/,
        use: [
          {
            loader: 'file-loader', // 使用file-loader来处理图片文件
            options: {
              esModule: false,
              name: '[name].[ext]', // 保持原始文件名和扩展名
              outputPath: 'models/' // 输出到指定目录下的images文件夹
            }
          }
        ],
        type: "javascript/auto", 
      },
      // html文件中src图片资源
      {
        test: /\.html$/,
        loader: "html-loader",
      },
      // 导入less文件
      {
        test: /\.less$/,
        use: [
          {
            loader: "style-loader", // creates style nodes from JS strings
          },
          {
            loader: "css-loader", // translates CSS into CommonJS
          },
          {
            loader: "less-loader", // compiles Less to CSS
          },
        ],
      },
      /*配置url*/

      /*配置es6转es5*/
      {
        test: /\.js$/,
        /*排除以下文件夹转换*/
        exclude: /(node_modules|bower_components)/,
        // use: {
        //   loader: "babel-loader",
        //   options: {
        //     cacheDirectory: true,
        //   },
        // },
        loader: "babel-loader",
          options: {
            cacheDirectory: true,
          },
      },
      /*配置vue-loader和vue-template-compiler*/

      {
        test: require.resolve("jquery"), //require.resolve 用来获取模块的绝对路径
        use: [
          {
            loader: "expose-loader",
            options: "jQuery",
          },
          {
            loader: "expose-loader",
            options: "$",
          },
        ],
      },
    ],
  },

  // 配置webpack的插件
  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    new Dotenv(),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
    }),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: true,
      __VUE_PROD_DEVTOOLS__: false,
    }),
  ],
};
