import { defineStore } from "pinia";

export const pageStatusStore = defineStore("pageStatus", {
  state: () => {
    return {
      mainPageScrollHeight: 0,              //记录主页面滚动的位置进行实时保存进行后续的更新
      modelLoadingStatus: true,            //记录模型加载状态
    };
  },
  actions: {
    
  },
  getters: {
    
  },
  persist: true,
});