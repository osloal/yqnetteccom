import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js';
import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import * as THREE from 'three';
import { GUI } from 'lil-gui';
import { ElLoading } from 'element-plus'
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ViewHelper } from '../../lib/ViewHelper';
import { pageStatusStore } from '../../store/pageStatus';
const modelStatus = pageStatusStore();

class House extends GlobalConfig {
    public controls: OrbitControls;
    public helper:ViewHelper|null;
    private object: any;
    public Loading: any;
 
    constructor() {
        super()
        this.rendererCoord();
        this.configCamera();
        this.configAmbientLight();
        this.configDirectionalLight();
        modelStatus.modelLoadingStatus = true;
        this.controls = new OrbitControls(this.camera, this.render.domElement);
        this.controls.maxPolarAngle = Math.PI * 0.5;
        this.controls.minDistance = 1;
        this.controls.maxDistance = 2000;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.5;
        this.controls.enablePan = true;

        //更新环境光
        this.ambientLight.intensity = 0.45;
        this.ambientLight.position.set(0, 0, 800);

        this.helper = null;

        //增加hdr贴图
        this.changeEnvMap('https://3d.shixianjia.com/hdr_sky/hdr/pizzo_pernice_puresky_4k.hdr')
        this.initModel()
        this.initObj()
        this.initGUI()
        this.animate()
        // 方便在页面检查代码
        window.House = this;

    }
    private animate(){                                        
        requestAnimationFrame(() => this.animate());
        this.render.render(this.scene, this.camera);
        this.helper?.render();
    }
    private changeEnvMap(url: string) {
        new RGBELoader().setDataType(THREE.HalfFloatType).load(
            url,
            t => {
                t.mapping = THREE.EquirectangularReflectionMapping;
                this.scene.background = t;
            },
        );
    }
    private initGUI(){
      
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }
    private initModel() {
        // let _this = this
        // var loader = new GLTFLoader();
        
        // loader.load(
        //     process.env.BASE_API_URL+this.gui.model_path, 
        //     gltf => {
        //         _this.object = gltf.scene;
        //         if(modelStatus.modelLoadingStatus){
        //             _this.scene.add(_this.object);
        //             _this.object.scale.set(0.13,0.13,0.13); 
        //             _this.object.position.set(0,0,0);
        //         }else{
        //             return false
        //         }
        //     },
        //     function(xhr){
        //         const progress = Math.floor(xhr.loaded / xhr.total * 100);
        //         if(modelStatus.modelLoadingStatus){
        //             _this.onLoadingMask(progress)
        //             if(xhr.loaded / xhr.total * 100 == 100){
        //                 _this.offLoadingMask()
        //             }
        //         }else{
        //             _this.offLoadingMask()
        //         }
        //     },
        //     (error) => {
        //         console.error('模型终止加载或资源异常:', error);
        //     }
        // );
    }
    private initObj(){
        // 创建球体几何体
        const radius = 1; // 半径
        const widthSegments = 32; // 水平分段数
        const heightSegments = 16; // 垂直分段数
        const sphereGeometry = new THREE.SphereGeometry(radius, widthSegments, heightSegments);

        // 创建材质
        const material = new THREE.MeshBasicMaterial({ color: 0x0000ff }); 

        // 创建球体网格
        const sphereMesh = new THREE.Mesh(sphereGeometry, material);

        // 将球体网格添加到场景中
        this.scene.add(sphereMesh);
        console.log(sphereMesh)
    }
    public onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.render.setSize(window.innerWidth, window.innerHeight);
    }
    public cleanup(){
       this.clearup()
    }
    public onLoadingMask(progress:number){
        this.Loading = ElLoading.service({
            lock: true,
            text: '正在加载模型',
            background: 'rgba(0, 0, 0, 0.7)',
        })
        this.Loading.setText('正在加载模型：['+progress+'%]');
    }
    public offLoadingMask(){
        this.Loading.close()
    }
}
export default House 