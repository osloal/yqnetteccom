import './Utils/Style/Global.less'
import { createApp } from 'vue';
import router from './router';
import App from './App.vue';
import './lib/rem.js'
import 'element-plus/dist/index.css'


import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

(() => {
    const app = createApp(App);
    app.use(router);
    app.use(pinia);
    app.mount('#app');
    console.log(
        '%c 🚀欢迎进入设计网站官网',
        'color:pink;font-size:20px;font-weight:bold;'
    );
    againstDebugger();
    getIp()
})()
function againstDebugger() {
    // @ts-ignore
    // setInterval(()=>{
    //     debugger
    // },10)
}
function getIp(){
    fetch('https://api.ipify.org/').then(
        r => r.text()
    ).then(console.log);

// -> 144.22.31.81
// (The output will vary for everyone)

}