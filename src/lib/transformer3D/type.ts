/**@description 选择的箭头 */
export enum ESelectArrow {
  NONE = 0,
  ARROWX,
  ARROWY,
  ARROWZ,
  RINGX,
  RINGY,
  RINGZ
}

/**@description 传入的stuff */
export interface Istuff {
  m_Object3D: THREE.Object3D
  isLock: boolean
  setLock: (isLock: boolean) => void
  destory: () => void
  setHighLight:(val:boolean)=>void;
  setVisible: (isV: boolean) => void
  move?: (vect3: THREE.Vector3) => void
  rotate?: (vect3: THREE.Vector3) => void
  mirrorX?: () => void
  mirrorY?: () => void
  mirrorZ?: () => void
  copy?: () => void
}

export interface ITransformer3D {
  mousedown: (event: MouseEvent) => boolean
  mousemove: (event: MouseEvent) => boolean
  mouseup: (event: MouseEvent) => void
}
