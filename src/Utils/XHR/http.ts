import request from './request'

interface http {
    get(url:string,params:any,headers:any):void,
    post(url:string,params:any,headers:any):void
}

const http:http = {
    get(url,params,headers){
        const config = {
            method:'GET',
            url:url,
            params:params ? params :{},
            headers: headers ? headers: {}
        }
        return request(config)
    },
    post(url,params,headers){
        const config = {
            method:'GET',
            url:url,
            params:params ? params :{},
            headers: headers ? headers: {}
        }
        return request(config)
    }
}
export default http