import axios from 'axios'
import { ElMessage } from 'element-plus'

const service = axios.create({
    baseURL:'localhost:2333',
    withCredentials:false,//异步请求携带cookie
    timeout:10*1000,
    headers:{"content-type":"application/json"}
})

//配置请求拦截器
service.interceptors.request.use(config => {
    config.headers.Authorization = localStorage.getItem("token") || '';
    config.headers['Content-Type'] = 'application/json';
    return config
})

//配置相应拦截器
service.interceptors.response.use(response =>{
    if(response.status == 200){
        return response.data //根据借口实际返回的字段进行取值
    }
},error => {
    if(error.response.data.status == 422){
        if(error.response.data.code == 500){
            ElMessage({
                message:error.response.data.message || '服务器内部错误',
                type:'error'
            })
        }
    }
})

export default service