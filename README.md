# YQNetTecCom


## Getting started

1、npm i (npm install)

2、npm run start

## Integrate with your tools
vsCode
Volar
GitHub Copilot

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Name
yqOffcialNet

## Roadmap
展示3D山体模型、模型切换

## Contributing
dwh
wcq
## Authors
前端ui部分：dwh
3D展示处理部分：wcq
## License
For open source projects, say how it is licensed.

## Project status
developing


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/osloal/yqnetteccom.git
git branch -M main
git push -uf origin main
```